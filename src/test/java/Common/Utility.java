package Common;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

public class Utility {
 
	public WebDriver driver;
	
	public Properties loadconfig(String sPath) {
		Properties prop=new Properties();
		try {
			FileInputStream fs = new FileInputStream(sPath);
			prop.load(fs);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return prop;
	}
	
	/*public Properties initproperities(String className) {
		
	}*/
}
